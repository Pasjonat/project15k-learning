package com.itreflection.project15.learning.task3.impl;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.google.common.collect.Lists;

@RunWith(MockitoJUnitRunner.class)
public class BadWordsFilterImplTest {

  @InjectMocks
  private BadWordsFilterImpl badWordsFilter;

  @Test
  public void shouldNotFilterWhenNoBadWords() throws Exception {
    //given
    List<String> words = Lists.newArrayList("blue", "funny", "car");

    //when
    List<String> result = badWordsFilter.filter(words);

    //then
    assertThat(result).hasSize(words.size());
  }

}