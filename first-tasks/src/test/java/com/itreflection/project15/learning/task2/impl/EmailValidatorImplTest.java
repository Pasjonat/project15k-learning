package com.itreflection.project15.learning.task2.impl;

import static org.fest.assertions.Assertions.assertThat;

import com.itreflection.project15.learning.task2.api.model.Severity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.itreflection.project15.learning.task2.api.model.ValidationDto;
import sun.reflect.annotation.ExceptionProxy;

@RunWith(MockitoJUnitRunner.class)
public class EmailValidatorImplTest {

  @InjectMocks
  private EmailValidatorImpl emailValidator;

  @Test
  public void shouldHaveNoErrorsWhenValidEmail() throws Exception {
    //given
    String email = "test@email.com";

    //when
    ValidationDto result = emailValidator.validate(email);

    //then
    assertThat(result).isNotNull();
    assertThat(result.getErrors()).isEmpty();
  }

  @Test
  public void shouldHaveErrorsWhenFirstPartIsTooLong () throws Exception{
    //given
    String email = "wswswswswswswswswswsws@example.test";

    //when
    ValidationDto result = emailValidator.validate(email);

    //then
    assertThat(result).isNotNull();
    assertThat(!result.getErrors().isEmpty());
  }
  @Test
  public void shouldHaveErrorsWhenFirstPartDomainIsTooLong () throws Exception{
    //given
    String email = "test@longExample.test";

    //when
    ValidationDto result = emailValidator.validate(email);

    //then
    assertThat(result).isNotNull();
    assertThat(!result.getErrors().isEmpty());
  }
  @Test
  public void shouldHaveErrorsWhenSecondPartDomainIsTooLong () throws Exception{
    //given
    String email = "example@test.tooLong";

    //when
    ValidationDto result = emailValidator.validate(email);

    //then
    assertThat(result).isNotNull();
    assertThat(!result.getErrors().isEmpty());
  }
  @Test
  public void shouldHaveErrorsWhenAtIsMoreThanOne () throws Exception{
    //given
    String email = "test@test@test.test";

    //when
    ValidationDto result = emailValidator.validate(email);

    //then
    assertThat(result).isNotNull();
    assertThat(!result.getErrors().isEmpty());
  }



}