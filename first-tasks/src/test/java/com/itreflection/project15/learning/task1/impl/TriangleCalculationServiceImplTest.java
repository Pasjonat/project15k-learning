package com.itreflection.project15.learning.task1.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.slf4j.LoggerFactory.getLogger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.itreflection.project15.learning.task1.api.TriangleParameterException;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class TriangleCalculationServiceImplTest {

    private final Logger logger = getLogger(TriangleCalculationServiceImplTest.class);

    @InjectMocks
    private TriangleCalculationServiceImpl triangleCalculationService;



    @Test
    public void shouldNoExceptionWhenBuildTriangle() throws Exception {
        //given
        int a = 6, b = 5, c = 2;

        //when
        double result = triangleCalculationService.calculatePerimeter(a, b, c);

        //then
        assertThat(result).isEqualTo(13);
    }

    @Test
    public void shouldHaveExceptionWhenVariableAEqualsThanZero() throws Exception {
        //given
        int a = 0, b = 2, c = 2;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
        } catch (Exception e) {

            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("a<=0");
        }
    }
    @Test
    public void shouldHaveExceptionWhenVariableBEqualsThanZero() throws Exception {
        //given
        int a = 4, b = 0, c = 2;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
        } catch (Exception e) {

            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("b<=0");
        }
    }
    @Test
    public void shouldHaveExceptionWhenVariableCEqualsThanZero() throws Exception {
        //given
        int a = 3, b = 4, c = 0;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
        } catch (Exception e) {

            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("c<=0");
        }
    }

    @Test
    public void shouldHaveExceptionWhenVariableALessZero() throws Exception {
        //given
        int a = -15, b = 2, c = 2;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
        } catch (Exception e) {

            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("a<=0");
        }
    }
    @Test
    public void shouldHaveExceptionWhenVariableBLessZero() throws Exception {
        //given
        int a = 4, b = -2, c = 2;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
        } catch (Exception e) {

            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("b<=0");
        }
    }
    @Test
    public void shouldHaveExceptionWhenVariableCLessZero() throws Exception {
        //given
        int a = 3, b = 4, c = -5;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
        } catch (Exception e) {

            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("c<=0");
        }
    }

    @Test
    public void shouldHaveExceptionWhenSidesIsThatSameNumber () throws Exception {
        //given
        int a = 1, b = 1, c = 1;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
        } catch (Exception e) {

            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("From given sides can't build a Triangle. "+a+" side is larger than "+b+" and "+c+"");
        }
    }
    @Test
    public void shouldHaveExceptionWhenSideAIsTooLong () throws Exception {
        //given
        int a = 10, b = 2, c = 4;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
        } catch (Exception e) {

            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("From given sides can't build a Triangle. "+a+" side is larger than "+c+" and "+b+"");
        }
    }
    @Test
    public void shouldHaveExceptionWhenSideBIsTooLong () throws Exception {
        //given
        int a = 3, b = 15, c = 4;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
        } catch (Exception e) {

            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("From given sides can't build a Triangle. "+b+" side is larger than "+c+" and "+a+"");
        }
    }
    @Test
    public void shouldHaveExceptionWhenSideCIsTooLong () throws Exception {
        //given
        int a = 3, b = 10, c = 50;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
        } catch (Exception e) {

            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("From given sides can't build a Triangle. "+c+" side is larger than "+b+" and "+a+"");
        }
    }

}

