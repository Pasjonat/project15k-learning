package com.itreflection.project15.learning.task2.impl;

import com.itreflection.project15.learning.task2.api.EmailValidator;
import com.itreflection.project15.learning.task2.api.model.Error;
import com.itreflection.project15.learning.task2.api.model.Severity;
import com.itreflection.project15.learning.task2.api.model.ValidationDto;

public class EmailValidatorImpl implements EmailValidator {

    private ValidationDto validateEmail = new ValidationDto();
    private long errorNumber = 1;

    @Override
    public ValidationDto validate(String email) {
        String[] splitEmailAt = email.split("@");
        firstPartRules(splitEmailAt);
        chectAt(splitEmailAt);

        if (splitEmailAt.length > 2) {
            checkSecondPartFullEmail(splitEmailAt);
        }
        return validateEmail;
    }

    private void firstPartRules(String[] splitEmailAt) {
        if (splitEmailAt[0].length() > 10) {
            validateEmail.add(new Error(errorNumber, "First part email is more than 10 chars", Severity.ERROR));
        }
    }

    private void chectAt(String[] splitEmailAt) {
        if (splitEmailAt.length < 2) {
            validateEmail.add(new Error(errorNumber, "At is more than 1.", Severity.ERROR));
            errorNumber++;
        }
    }

    private void checkSecondPartFullEmail(String[] splitEmailAt) {
        String[] splitEmailFullStop = splitEmailAt[1].split(",");
        checkFirstPartOfDomain(splitEmailFullStop);
        checkSecondPartOfDomain(splitEmailFullStop);
    }


    private void checkFirstPartOfDomain(String[] splitEmailFullStop) {
        if (splitEmailFullStop[0].length() < 9) {
            validateEmail.add(new Error(errorNumber, "First part of domain is more than 8 chars", Severity.ERROR));
            errorNumber++;
        }
    }

    private void checkSecondPartOfDomain(String[] splitEmailFullStop) {
        for (String aSplitEmailFullStop : splitEmailFullStop) {
            if (aSplitEmailFullStop.length() != 4 || aSplitEmailFullStop.length() != 2) {
                validateEmail.add(new Error(errorNumber, "Second part of domain isnt 2 or 4 chars", Severity.ERROR));
                errorNumber++;
            }
        }
    }

}
