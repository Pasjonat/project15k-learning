package com.itreflection.project15.learning.task3.api;

import java.util.List;

public interface BadWordsFilter {

  List<String> filter(List<String> words);

}
