package com.itreflection.project15.learning.task1.impl;

import com.itreflection.project15.learning.task1.api.TriangleCalculationService;
import com.itreflection.project15.learning.task1.api.TriangleParameterException;

import java.util.Arrays;
import java.util.HashSet;

public class TriangleCalculationServiceImpl implements TriangleCalculationService {

    @Override
    public double calculatePerimeter(int a, int b, int c) {
        calculationPerimeterOfTriangle(a, b, c);
        return a + b + c;
    }

    private void calculationPerimeterOfTriangle(int a, int b, int c) {
        parameterZeroOrLess(a, "a<=0");
        parameterZeroOrLess(b, "b<=0");
        parameterZeroOrLess(c, "c<=0");
        triangleInequality(a, b, c);
    }

    private void parameterZeroOrLess(int param, String msg) {
        if (param <= 0) {
            throw new TriangleParameterException(String.format(msg, param));
        }
    }

    private void triangleInequality(int a, int b, int c) {
        int[] sides = {a, b, c,};
        Arrays.sort(sides);
        if (sides[2] >= sides[1] + sides[0]) {
            throw new TriangleParameterException(String.format("From given sides can't build a Triangle. %d side is larger than %d and %d", sides[2], sides[1], sides[0]));
        }
    }

}
