package com.itreflection.project15.learning.task4.api.service;

import com.itreflection.project15.learning.task4.api.model.Form;

public interface FormComparisionService {

   int comparePerimeter(Form form1, Form form2);

   int compareField(Form form1, Form form2);
}
