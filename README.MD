# 1. Implement function to calculate of a triangle write JUnit tests for it. 
```
com.itreflection.project15.learning.task1
```
- method to implement: com.itreflection.project15.learning.task1.impl.TriangleCalculationServiceImpl.calculatePerimeter
- test class in which tests should be implemented:  com.itreflection.project15.learning.task1.impl.TriangleCalculationServiceImplTest
- example of simple JUnit can be found here: com.itreflection.project15.learning.first.ClassATest

When writing a test please please take into account that parameter can be any integer.
e.g. [1,2,3], [-1,-2-3], [0,0,0], [1,1,1]


# 2. Implement function which validate email address
```
com.itreflection.project15.learning.task2
```
- rules for validation: email should have only one '@' and at least one ".", max No. of email account should be 10 characters, first part of domain can have up to 8 chars, second part of domain should have 2 or 4 characters 
- valid ones examples: email@test.com, email.@test1.test.go
- not valid examples: @mail@test.com, 12345678901@test.com, email@123456789.com
Please note that normal email addresses can have more characters, however to make easier for testing I reduce No. of possible characters.

Implementation and tests should be done there:
```
com.itreflection.project15.learning.task2.impl.EmailValidatorImpl
com.itreflection.project15.learning.task2.impl.EmailValidatorImplTest
```

# 3. Implement filter for given bad words longer 
```
com.itreflection.project15.learning.task3
```
Lets assume that our bad words will be "shit", "fuck".
- implementation should remove all words like: shit, Shit, sHit etc. In other words it should be case insensitive.
Implementation and tests should be done there:
```
com.itreflection.project15.learning.task3.impl.BadWordsFilterImpl
com.itreflection.project15.learning.task3.impl.BadWordsFilterImplTest
```

# 4. Implement service to compare forms
```
com.itreflection.project15.learning.task4
```
Implement classes Triangle and Rectangle
Implement test
```
com.itreflection.project15.learning.task4.impl.FormComparisionServiceImplTest
```

